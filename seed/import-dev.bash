#!/usr/bin/env bash
:: Change directory to this file bath before run
mongoimport -d dargee-dev --drop -c user --jsonArray < users.json
mongoimport -d dargee-dev --drop -c admin --jsonArray < admins.json
mongoimport -d dargee-dev --drop -c bookings --jsonArray < bookings.json
mongoimport -d dargee-dev --drop -c appointments --jsonArray < appointments.json
mongoimport -d dargee-dev --drop -c contactUs --jsonArray < contactUs.json
mongoimport -d dargee-dev --drop -c bookingDays --jsonArray < bookingDays.json
mongoimport -d dargee-dev --drop -c bookingHours --jsonArray < bookingHours.json
