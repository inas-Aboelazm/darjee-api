'use strict';
const nodemailer = require('nodemailer');
const mailTransport = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    requireTLS: true,
    service: 'Gmail',
    auth: {
        user: ' ', //sender email
        pass: ' ' //sender password
    }
  });
module.exports = function(Booking) {
    Booking.observe('after save', function(context, next) {
        var email = context.instance.email;
        var subject = 'Dargee Booking Confirmation';
        var body = `Thanks ${context.instance.name}, We will call you to confirm your Booking`;
        const mailOptions = {
            from: ' ', //sender email
            to: email, //sender password
            subject: subject,
            html: body
        };   
        mailTransport.sendMail(mailOptions, (error, info) => {
            if (error) {
                console.log("message----------",error.message);
            }else{
              console.log('success---booking');
            }
            next()
        });
    });

};