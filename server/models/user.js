'use strict';
const nodemailer = require('nodemailer');
var mailer = require('./../../utils/sendMail');
var app = require('../../server/server');

const mailTransport = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 587,
  secure: false,
  requireTLS: true,
  service: 'Gmail',
  auth: {
      user: '', //sender email
      pass: '' //sender password
  }
});
module.exports = function (User) {
    User.observe('after save', function(context, next) {
      if(context.isNewInstance){
        var subject = 'Darjee - Email Verification';
        var email = context.instance.email;
        var body = `Thanks ${context.instance.name} for registeration in Darjee, \n
          please click on the link to verify your email. \n
        <a href='http://localhost:3000/api/users/confirmEmail?id=${context.instance.id}'>Confirm Your Email</a>`;
        const mailOptions = {
          from: '', //sender email
          to: email, //sender password
          subject: subject,
          html: body
      };       
        mailTransport.sendMail(mailOptions, (error, info) => {
          if (error) {
              console.log("message----------",error.message);
          }else{
            console.log('success');
          }
          next()
      });
    }
    });

    User.remoteMethod('confirmEmail', {
        accepts: [{arg: 'id', type: 'string', required: true}],
        returns: {arg: 'data', type: 'object', root: true},
        http: {path: '/confirmEmail', verb: 'get'}});

        User.confirmEmail = function(id, next) {
            User.findOne({where: {id: id}}, function(err, result) {
              if (err) {
                console.log('err---->', err)
                next(err, result);
              } else {
                result.isVerified = true;
                console.log('result---2--->', result)
                next(null,result)
                User.patchOrCreate(result, false, function(err, res) {
                  console.log('result---confirm--->', res)
                  next(null,res)
                });
              }
            });
          };

          User.remoteMethod('resendEmail', {
            accepts: [{arg: 'id', type: 'string'}],
            returns: {},
            http: {path: '/resendEmail', verb: 'get'}});
    
            User.resendEmail = function(id, callback) {
                User.findOne({where: {id: id}}, function(err, result) {
                  if (err) {
                    callback(err);
                  } else {
                    var subject = 'Darjee - Email Verification';
                    var email = result.email;
                    var body = `Thanks ${result.name} for registeration in Darjee, \n
                      please click on the link to verify your email. \n
                    <a href='http://localhost:4200/verified/${id}'>Confirm Your Email</a>`;
                    const mailOptions = {
                      from: ' ', //sender email
                      to: email, //sender password
                      subject: subject,
                      html: body
                  };       
                    mailTransport.sendMail(mailOptions, (error, info) => {
                      if (error) {
                          console.log("message-----resend-----",error.message);
                      }else{
                        console.log('success_resend');
                      }
                      next()
                  });
                }
                });
              };

    
};
