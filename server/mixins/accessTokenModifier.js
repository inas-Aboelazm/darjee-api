module.exports = function (Model, options) {
  var app = require('../../server/server');
  Model.afterRemote('login', function setLoginCookie(context, accessToken, next) {
    console.log('context', context.result, accessToken);
    if (accessToken !== null) {
      if (accessToken.id !== null) {
        var type = Model.name[0].toUpperCase() + Model.name.slice(1);

        app.models[type].findById(accessToken.userId)
          .then(function (user, err) {
            if (!user.blocked) {
              return user;
            }
          })
          .then(function (user) {
            if (user) {
              return app.models.AccessToken.findById(accessToken.id);
            }
          })
          .then(function (token, err) {
            if (token) {
              token.updateAttribute('type', type);
              token.save().then(function () {
                next();
              });
            } else {
              next({statusCode: 403, message: 'account is blocked'});
            }
          });
      }
    } else {
      return next();
    }
  });
};
