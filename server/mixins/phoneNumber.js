var axios = require('axios');
var moment = require('moment');

module.exports = function (Model, options) {
    
    Model.remoteMethod('phoneVerify', {
        accepts: [
            {arg: 'passCode', type: 'string', required: true},
            {arg: 'mobile', type: 'string', required: true},
        ],
        returns: {type: 'Object', root: true},
        http: {path: '/phone/verifyCode'}
    });
    Model.remoteMethod('sendActivationCode', {
        accepts: [
            {arg: 'options', type: 'object', http: 'optionsFromRequest'}
        ],
        returns: {type: 'Object', root: true},
        http: {path: '/phone/sendActivationCode'}
    });

    Model.remoteMethod('forgetPassword', {
        accepts: [
            {arg: 'mobile', type: 'string'}
        ],
        returns: {type: 'Object', root: true},
        http: {path: '/phone/forgetPassword'}
    });

    Model.remoteMethod('resetPassword', {
        accepts: [
            {arg: 'mobile', type: 'string'},
            {arg: 'vCode', type: 'string'},
            {arg: 'password', type: 'string'},
        ],
        returns: {type: 'Object', root: true},
        http: {path: '/phone/resetPassword'}
    });

    function genCode() {
        return Math.floor(Math.random() * 999999).toString();
    }

    function sendSMSVerificationCode(phone, code, message) {
        // if (phone.indexOf('0') == 0) {
        //     phone = phone.substring(1);
        // }
        //phone = '00966' + phone;
        const aOptions = {
            'method': 'POST',
            'url': 'http://api.otsdc.com/rest/Messages/Send',
            data: {
                "AppSid": "RUTUXy_IPpxKCWkVDxD5j3_MFm0h6z",
                "Recipient": phone,
                "Body": (message || ' تم تسجيل عضويتك بنجاح، كود التفعيل ') + ' ' + code,
            }
        };
        axios(aOptions)
            .then(function (res) {
                console.log(' SMS api respond ', res.data);
            })
            .catch(function (error) {
                console.log('send SMS', error);
            })
    }


    Model.sendActivationCode = function (rOptions, cb) {

        if (rOptions && rOptions.accessToken) {
            const token = rOptions && rOptions.accessToken;
            const userId = token && token.userId;
            Model.findById(userId,
                function (err, result) {
                    if (result) {
                        var diff = 0;
                        if (result.passCodeTime) {
                            diff = moment().diff(moment(result.passCodeTime), 'seconds');
                        }
                        if (result.passCodeTime && diff < 60) {
                            cb({message: 'برجاء الانتظار ' + (60 - diff) + ' ثانية قبل المحاولة مرة أخرى', statusCode: 400});
                        } else {
                            const passCode = genCode();
                            sendSMSVerificationCode(result[options.phoneProp], passCode);
                            result.passCode = passCode;
                            result[options.phoneVerifyProp] = false;
                            result.passCodeTime = moment();

                            result.save().then(function (u, err) {

                                cb(err, u);
                            });
                        }
                    } else {
                        cb({message: 'user is not found', statusCode: 404});
                    }

                })

        } else {
            cb({message: 'token is missing', statusCode: 403});
        }
    };
    Model.phoneVerify = function (passCode, mobile, callback) {
        Model.findOne({
            where: {
                passCode: passCode,
                [options.phoneVerifyProp]: false,
                [options.phoneProp]: mobile,
            }
        }, function (err, result) {
            if (result) {
                result[options.phoneVerifyProp] = true;
                result.unsetAttribute('passCode');
                result.unsetAttribute('passCodeTime');

                result.save().then(function (u, err) {
                    u.createAccessToken(500, function (er1, token) {
                        console.log('token', token)
                        callback(err, token);
                    })

                });
            } else {
                console.log('invalid token')
                callback({message: 'invalid token', statusCode: '404'});
            }

        });
    };
    Model.forgetPassword = function(mobile, cb) {
        Model.findOne({
            where: {username: mobile}}, function(err, result) {
            if (result) {
                var diff = 0;
                if (result.vCodeTime) {
                    diff = moment().diff(moment(result.vCodeTime), 'seconds');
                }
                if (result.vCodeTime && diff < 300) {
                    if (diff > 240) {
                        cb({
                            message: 'برجاء الانتظار ' + (300 - diff) +
                            ' ثانية قبل المحاولة مرة أخرى', statusCode: 400});
                    } else {
                        cb({
                            message: 'برجاء الانتظار ' + (6 - (Math.ceil(diff / 60))) +
                            ' دقيقة قبل المحاولة مرة أخرى', statusCode: 400});
                    }
                } else {
                    const vCode = genCode();
                    sendSMSVerificationCode(mobile, vCode,
                        ' تم طلب استرجاع كلمة المرور، كود التفعيل هو ');
                    result.vCode = vCode.toString();
                    result.vCodeTime = moment();
                    result.save(function (err, u) {
                        cb(err, 'sent');
                    });
                }
            } else {
                cb({message: 'لقد قمت بكتابة رقم جوال غير مسجل.' +
                ' برجاء إدخال رقم جوال مسجل', statusCode: '404'});
            }
        });
    }

    Model.resetPassword = function (mobile, vCode, password, cb) {
        Model.findOne({
            where: {
                username: mobile,
                vCode: vCode
            }
        }, function (err, result) {

            if (result) {
                result.unsetAttribute('vCode');
                result.unsetAttribute('vCodeTime');
                result.updateAttribute('password', Model.hashPassword(password));
                result.save(function (err, u) {
                    cb(err, u);
                });
            } else {

                cb({message: 'not found', statusCode: '404'});
            }
        });
    }

    Model.beforeRemote("*.patchAttributes", function (context, unused, next) {
        console.log('patchAttributes', context.instance, context.args.data)
        var user = context.instance;
        if (context.args.data.mobile) {
            Model.findOne({where: {mobile: context.args.data.mobile}},
                function(err, result) {
                    if (result && context.args.data.mobile === result.mobile &&
                        context.args.data.mobile !== user.mobile) {
                        next({'statusCode': 400,
                            'message': 'هذا الجوال لديه حساب أخر بالفعل.'});
                    } else {
                        if (user && context.args.data) {
                            if (context.args.data.mobile &&
                                context.args.data.mobile !== user.mobile) {
                                const passCode = genCode();
                                user.updateAttribute('passCode', passCode);
                                user.updateAttribute('username',
                                    context.args.data[options.phoneProp]);
                                user.updateAttribute(options.phoneVerifyProp, false);
                                user.passCodeTime = moment();
                                sendSMSVerificationCode(context.args.data[options.phoneProp],
                                    passCode);
                                user.save().then();
                            }
                        }
                        next();
                    }
                });
        } else {
            if (user && context.args.data) {
                if (context.args.data.mobile &&
                    context.args.data.mobile !== user.mobile) {
                    const passCode = genCode();
                    user.updateAttribute('passCode', passCode);
                    user.updateAttribute('username',
                        context.args.data[options.phoneProp]);
                    user.updateAttribute(options.phoneVerifyProp, false);
                    user.passCodeTime = moment();
                    sendSMSVerificationCode(context.args.data[options.phoneProp],
                        passCode);
                    user.save().then();
                }
            }
            next();
        }
    });

    Model.observe('before save', function (ctx, next) {
        const model = ctx.instance;
        if (ctx.isNewInstance) {
            if (model[options.phoneProp])
                ctx.instance.username = ctx.instance[options.phoneProp];
        }
        next();
    });
}
;
