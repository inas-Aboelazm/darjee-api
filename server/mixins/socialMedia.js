// var axios = require('axios');
// var moment = require('moment');

module.exports = function (Model, options) {
    Model.remoteMethod('googlePlus', {
        accepts: [
            {arg: 'accessToken', type: 'string', required: true},
            {arg: 'email', type: 'string', required: true},
            {arg: 'userId', type: 'string', required: true},
            {arg: 'name', type: 'string', required: true},
            {arg: 'avatar', type: 'string', required: true},
            {arg: 'location', type: 'Object', required: true}
        ],
        returns: {type: 'Object', root: true},
        http: {path: '/login/google'}
    });

    Model.remoteMethod('facebook', {
        accepts: [
            {arg: 'accessToken', type: 'string', required: true},
            {arg: 'email', type: 'string', required: true},
            {arg: 'userId', type: 'string', required: true},
            {arg: 'name', type: 'string', required: true},
            {arg: 'avatar', type: 'string', required: true},
            {arg: 'location', type: 'Object', required: true}
        ],
        returns: {type: 'Object', root: true},
        http: {path: '/login/facebook'}
    });

    function genCode() {
        return Math.floor(Math.random() * 999999).toString();
    }

    Model.googlePlus = function (accessToken, email, userId, name, avatar, location, callback) {
        console.log('googlePlus', accessToken, email, userId, name, avatar, location);
        Model.findOne({
            where: {
                $or: [{'SM.accessToken': accessToken}, {'SM.userId': userId}],
                'SM.name': 'google'
            }
        }, function (err, u) {
            if (u) {
                u.createAccessToken(1209600, function (er1, token) {
                    console.log('token', token);
                    callback(err, token);
                })
            } else {
                var password = genCode();
                Model.create({
                    userName: email,
                    email: email,
                    SM: {accessToken: accessToken, name: 'google', userId: userId},
                    password: password,
                    name: name,
                    avatar: avatar,
                    location: location
                }, function (err, user) {
                    if (!err) {
                        user.createAccessToken(1209600, function (er1, token) {
                            console.log('token', token);
                            callback(err, token);
                        })
                    } else {
                        callback(err, user);
                    }
                })
            }

        });
    };

    Model.facebook = function (accessToken, email, userId, name, avatar, location, callback) {
        console.log('facebook', accessToken, email, userId, location);
        Model.findOne({
            where: {
                $or: [{'SM.accessToken': accessToken}, {'SM.userId': userId}],
                'SM.name': 'facebook'
            }
        }, function (err, u) {
            if (u) {
                u.createAccessToken(1209600, function (er1, token) {
                    console.log('token', token);
                    callback(err, token);
                })
            } else {
                var password = genCode();
                Model.create({
                    userName: email,
                    email: email,
                    SM: {accessToken: accessToken, name: 'facebook', userId: userId},
                    password: password,
                    name: name,
                    avatar: avatar,
                    location: location
                }, function (err, user) {
                    if (!err) {
                        user.createAccessToken(1209600, function (er1, token) {
                            console.log('token', token);
                            callback(err, token);
                        })
                    } else {
                        callback(err, user);
                    }
                })
            }

        });
    };
};
