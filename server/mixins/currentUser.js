module.exports = function (Model, options) {
  // Model is the model class
  // options is an object containing the config properties from model definition
  var app = require('../../server/server');
  Model.observe('before save', function (ctx, next) {
    if (ctx.isNewInstance) {
      app.models.User.findById(ctx.options.accessToken.userId, function (err, user) {
          if (err) return next(err);
          if (ctx.instance) {
            user.unsetAttribute('password');
            ctx.instance.user = user;
            ctx.instance.userId = ctx.options.accessToken.userId;
          }
          next();
        });
    } else {
      next();
    }
  });


};
