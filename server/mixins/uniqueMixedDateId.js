'use strict';
var moment = require('moment');

module.exports = function(Model) {
  Model.observe('before save', function(ctx, next) {
    if (ctx.instance) {
      var iid = moment().format('YYMMDD') +
        Math.floor(Math.random() * 9999).toString();
      ctx.instance.iid = iid;
    }
    next();
  });
};
