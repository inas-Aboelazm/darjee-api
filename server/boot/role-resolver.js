"use strict";

module.exports = function(app){
    var Role = app.models.Role;
    var Admin = app.models.Admin;

    function reject(cb) {
        process.nextTick(function() {
          cb(null, false);
        });
    }

    Role.registerResolver('admin', function(role, context, cb) {
        if(!context.accessToken)
            return reject(cb);
        var userId = context.accessToken.userId;
        if (!userId)
            return reject(cb);
        if (userId) {
          Admin.findById(userId, function(err, admin) {
              if (err || !admin)
                  return reject(cb);
              cb(null, true);
            });
        }
  });
};